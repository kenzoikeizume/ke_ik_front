const LOCAL_ENDPOINT = 'http://localhost:5000';
const WEB_ENDPOINT = 'https://keikapi.herokuapp.com';

export const CURRENT_ENDPOINT = LOCAL_ENDPOINT;

export const HTTP_POST = {
  method: "POST",
  headers: { "Content-Type": "application/json" },
  cache: "default"
};

export const HTTP_GET = {
  method: "GET",
  headers: { "Content-Type": "application/json" },
  cache: "default"
};

export const HTTP_DELETE = {
  method: "DELETE",
  headers: { "Content-Type": "application/json" },
  cache: "default"
};