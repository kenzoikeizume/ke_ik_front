import React from 'react';

import "./App.css";
import { Tabs, Tab } from '@blueprintjs/core';
import TableTournament from './components/TableTournament/TableTournament';
import Playoffs from './components/Playoffs/Playoffs';
import Bracket from './components/Bracket/Bracket';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.properties = {
      activePanelOnly: false,
      animate: true,
      navbarTabId: "Home",
      vertical: false
    }

    this.state = {
      teams: []
    }

    this.tableTournamentDone = this.tableTournamentDone.bind(this);
  }

  tableTournamentDone(teams) {
    this.setState({
      teams: teams
    })
  }

  render() {
    const { teams } = this.state;

    return (
      <div className="App">
        <Tabs
          animate={this.properties.animate}
          id="TabsExample"
          key={this.properties.vertical ? "vertical" : "horizontal"}
          renderActiveTabPanelOnly={this.properties.activePanelOnly}
          vertical={this.properties.vertical}
        >
          <Tab id="rx" title="Fase de grupos" panel={<TableTournament done={this.tableTournamentDone} />} />
          <Tab id="ng" title="Mata mata" panel={<Playoffs groupTeams={teams} />} />
          <Tab id="mb" title="Chaves" panel={<Bracket groupTeams={teams}/>} panelClassName="ember-panel" />
          <Tabs.Expander />
        </Tabs>
      </div>
    )
  }
}

export default App;
