import React from 'react';
import { Spinner } from '@blueprintjs/core';

export default class Loader extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hasValue: false,
      size: Spinner.SIZE_LARGE,
      value: 0.7,
    }
  }

  render() {
    const { hasValue, size, value } = this.state;

    return (
      <Spinner size={size} value={hasValue ? value : null} />
    )
  }
}