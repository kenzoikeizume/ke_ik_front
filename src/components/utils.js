export const TEAMS = [
  {
    "_id": "5d20f34aa839db514bf83eaa",
    "team": "Opal",
    "group": "Cantu",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a4a71aeb5014b188e",
    "team": "Patrice",
    "group": "Cantu",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aee5bef66c173db99",
    "team": "Trina",
    "group": "Cantu",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ada19f25475607541",
    "team": "Carver",
    "group": "Cantu",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a1bb4a9684ead37b5",
    "team": "Huber",
    "group": "Cantu",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a424fe56ae47ac604",
    "team": "Pratt",
    "group": "Larson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a1879abedd44d8782",
    "team": "Cleo",
    "group": "Larson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ad65dd4d48a8af31e",
    "team": "Cannon",
    "group": "Larson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aa54b816bdd91d01b",
    "team": "Sellers",
    "group": "Larson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a8c33a3dd824c9700",
    "team": "Campbell",
    "group": "Larson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a91cd2e543f4889fe",
    "team": "Kari",
    "group": "Johnson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a200cb8619355d763",
    "team": "Gaines",
    "group": "Johnson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a0f8adca7e57213cf",
    "team": "Bartlett",
    "group": "Johnson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ad450b794022514f1",
    "team": "Thornton",
    "group": "Johnson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a3a29808288d754a5",
    "team": "Warren",
    "group": "Johnson",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34af933f60e2303b27a",
    "team": "Ballard",
    "group": "England",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a857ae585e069d0f7",
    "team": "Summer",
    "group": "England",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ae38661eb1729131d",
    "team": "Jeannine",
    "group": "England",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ad0a98186d2e2d530",
    "team": "Morin",
    "group": "England",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a1ac4111f976b28ce",
    "team": "Guadalupe",
    "group": "England",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34afabfdbf170ab2ff5",
    "team": "Walker",
    "group": "Ingram",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a1d3d762535660241",
    "team": "Lisa",
    "group": "Ingram",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ad31468e10d7478fe",
    "team": "Woodard",
    "group": "Ingram",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a33c51e66a114a405",
    "team": "Petersen",
    "group": "Ingram",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a112204da91313b7c",
    "team": "Lorna",
    "group": "Ingram",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ad99331e0cb4bb887",
    "team": "Consuelo",
    "group": "Chambers",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34af8675b424c97f079",
    "team": "Anna",
    "group": "Chambers",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a6f3164e127d26fbc",
    "team": "Terri",
    "group": "Chambers",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ac07bc609e1872d28",
    "team": "Stacy",
    "group": "Chambers",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a787444b32d4832d4",
    "team": "Sabrina",
    "group": "Chambers",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a6f7eb730efb5d581",
    "team": "Ofelia",
    "group": "Parsons",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ac59b5b234bf82369",
    "team": "Esther",
    "group": "Parsons",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34afc8aec8b9574b0ed",
    "team": "Katie",
    "group": "Parsons",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a652ade138be04cff",
    "team": "Bertha",
    "group": "Parsons",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a3edab86bb1238cea",
    "team": "Cummings",
    "group": "Parsons",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aa2ee7d694a19d72e",
    "team": "Enid",
    "group": "Manning",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a3732de3a112bb395",
    "team": "Lowery",
    "group": "Manning",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a955449f12a52ee03",
    "team": "Cotton",
    "group": "Manning",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a837b05151539861d",
    "team": "Marie",
    "group": "Manning",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a5eb9e984ec869a3e",
    "team": "Hudson",
    "group": "Manning",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a7d041afc96ef9125",
    "team": "Henson",
    "group": "Mcfarland",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34afe26a7c936b33378",
    "team": "Parker",
    "group": "Mcfarland",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ac62eb02a728a8575",
    "team": "Lauri",
    "group": "Mcfarland",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34abc3b0732fcd47b2b",
    "team": "Dean",
    "group": "Mcfarland",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aeb53e76345cf68fb",
    "team": "Elsie",
    "group": "Mcfarland",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ad49964cc5992a372",
    "team": "Paula",
    "group": "Golden",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a9d6513c779d3a01c",
    "team": "Lorrie",
    "group": "Golden",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ae115927295e00331",
    "team": "Deana",
    "group": "Golden",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a036950e3f5836f1e",
    "team": "Larsen",
    "group": "Golden",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aa94e6f1021ce5545",
    "team": "Gilbert",
    "group": "Golden",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ae2a5b88c42014077",
    "team": "Vivian",
    "group": "Gentry",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aa1da68aaa6a32db5",
    "team": "Helga",
    "group": "Gentry",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ada959e06eb090116",
    "team": "Terrell",
    "group": "Gentry",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aef9cc644f15a5003",
    "team": "Evans",
    "group": "Gentry",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a47bff8525c854e41",
    "team": "Donna",
    "group": "Gentry",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a15da58bddcb835fd",
    "team": "Yates",
    "group": "Barlow",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ac448383719b5e177",
    "team": "Delacruz",
    "group": "Barlow",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a29d4efc8bd6f89d5",
    "team": "Rich",
    "group": "Barlow",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a8335da1de5aa58c5",
    "team": "Donaldson",
    "group": "Barlow",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a1a03913d2b684341",
    "team": "Wolfe",
    "group": "Barlow",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a16c9557adb5663d2",
    "team": "Maritza",
    "group": "Booker",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a479850272c47aa48",
    "team": "Fleming",
    "group": "Booker",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a647aef9d8942965f",
    "team": "Marguerite",
    "group": "Booker",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a4008df91802815ef",
    "team": "Levine",
    "group": "Booker",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ad389393e587e07da",
    "team": "Mclaughlin",
    "group": "Booker",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a9abcab41f0b109c5",
    "team": "Corinne",
    "group": "Freeman",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a9e816decd31bea0e",
    "team": "Toni",
    "group": "Freeman",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a28d59099fdc59426",
    "team": "Schwartz",
    "group": "Freeman",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34abf8df7c7844a860c",
    "team": "Claudine",
    "group": "Freeman",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a605210f4b0959fbc",
    "team": "Margie",
    "group": "Freeman",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a6ec08e59998f04e5",
    "team": "Meadows",
    "group": "Vincent",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aca4dc95b3e8a3055",
    "team": "Joan",
    "group": "Vincent",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a1c8597a6883f550b",
    "team": "Valentine",
    "group": "Vincent",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a123141876ff22bc4",
    "team": "Adela",
    "group": "Vincent",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a4563beff00027bd7",
    "team": "Florine",
    "group": "Vincent",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a55e922029437a32b",
    "team": "Love",
    "group": "Hardy",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34a4b4e70a8bab5fb80",
    "team": "Bright",
    "group": "Hardy",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34aa45f4d5ac7a07f72",
    "team": "Randi",
    "group": "Hardy",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34ae7d50b5158b082d2",
    "team": "Rosetta",
    "group": "Hardy",
    "points": 0,
    "victory": 0
  },
  {
    "_id": "5d20f34af698b138f5fca1bf",
    "team": "Mayo",
    "group": "Hardy",
    "points": 0,
    "victory": 0
  }
]