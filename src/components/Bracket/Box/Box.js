import React from 'react';

const Box = ({ firstTeam, lastTeam }) => {
  return (
    <div className="box">
      <span>{firstTeam}</span>
      <span>X</span>
      <span>{lastTeam}</span>
    </div>
  )
}

export default Box;