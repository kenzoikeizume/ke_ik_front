import React from 'react';
import Box from './Box/Box';
import { Button } from '@blueprintjs/core';
import Loader from '../Loader/Loader';
import { HTTP_GET, CURRENT_ENDPOINT } from '../../endpoint';

export default class Bracket extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      data: {
        octaves: [],
        quarters: [],
        semis: [],
        final: [],
        winner: []
      }
    }

    this.setStateTeams = this.setStateTeams.bind(this);
    this.getOctavesTeams = this.getOctavesTeams.bind(this);
    this.fiterValuesIds = this.fiterValuesIds.bind(this);
    this.playOctaves = this.playOctaves.bind(this);
    this.playQuarters = this.playQuarters.bind(this);
    this.playFinals = this.playFinals.bind(this);
  }

  componentDidMount() {
    this.getOctavesTeams();
  }

  componentDidUpdate(prevProps) {
    if(prevProps.groupTeams != this.props.groupTeams) {
      this.getOctavesTeams();
    }
  }

  getOctavesTeams() {
    this.setState({
      loaded: false
    })

    fetch(CURRENT_ENDPOINT + '/bracket', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then((data) => {
      this.setStateTeams(data);

      this.setState({
        loaded: true
      })
    })
  }

  setStateTeams(data) {
    const ordedData = data.sort((a, b) => {
      const order = -1;
      if (a.id > b.id) {
        return 1;
      }

      return order;
    })

    const values = ordedData.reduce((acc, value) => {
      if (value.quarter_id) {
        acc.octaves.push(value.quarter_id);
      }
      if (value.quarter_rival_id) {
        acc.octaves.push(value.quarter_rival_id);
      }
      if (value.semi_id) {
        acc.quarters.push(value.semi_id);
      }
      if (value.final_id) {
        acc.semis.push(value.final_id);
      }
      if (value.final_rival_id) {
        acc.final.push(value.final_rival_id);
      }
      if (value.winner_id) {
        acc.winner.push(value.winner_id);
      }

      return acc;
    }, {
        octaves: [],
        quarters: [],
        semis: [],
        final: [],
        winner: []
      })

    this.setState({
      data: {
        octaves: this.fiterValuesIds(values.octaves),
        quarters: this.fiterValuesIds(values.quarters),
        semis: this.fiterValuesIds(values.semis),
        final: this.fiterValuesIds(values.final),
        winner: this.fiterValuesIds(values.winner)
      }
    })
  }

  fiterValuesIds(arrayData) {
    const { groupTeams } = this.props;
    
    if(!groupTeams.length)
      return [];

    return arrayData.reduce((acc, value) => {
      return [...acc, groupTeams.find((valueGroup) => valueGroup.id === value).team]
    }, [])
  }

  playOctaves() {
    this.setState({
      loaded: false
    });

    fetch(CURRENT_ENDPOINT + '/quarter_bracket', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then(() => {
      this.playQuarters();
    })
  }

  playQuarters() {
    fetch(CURRENT_ENDPOINT + '/semi_bracket', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then(() => {
      this.playFinals();
    })
  }

  playFinals() {
    fetch(CURRENT_ENDPOINT + '/final_bracket', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then(() => {
      this.getOctavesTeams();
    })
  }

  render() {
    const { loaded, data } = this.state;

    const disableOctaves = !data.octaves[0] || !!data.quarters[0];

    return (
      <div>
        {loaded ?
          <div>
            <div style={{ margin: '10px' }}>
              <Button onClick={this.getOctavesTeams}>GET TIMES</Button>
              <Button onClick={this.playOctaves} disabled={disableOctaves}>JOGAR</Button>
            </div>
            <div style={{
              display: 'flex',
              flexWrap: 'wrap'
            }}>
              {data &&
                <div>
                  <div className="brackets">
                    <div className="quarter-finals-left">
                      <Box firstTeam={data.octaves[0]} lastTeam={data.octaves[1]} />
                      <Box firstTeam={data.octaves[2]} lastTeam={data.octaves[3]} />
                      <Box firstTeam={data.octaves[4]} lastTeam={data.octaves[5]} />
                      <Box firstTeam={data.octaves[6]} lastTeam={data.octaves[7]} />
                    </div>
                    <div className="semi-finals">
                      <div className="semi-finals-left">
                        <Box firstTeam={data.quarters[0]} lastTeam={data.quarters[1]} />
                        <Box firstTeam={data.quarters[2]} lastTeam={data.quarters[3]} />
                      </div>
                      <div className="final">
                        <Box firstTeam={data.semis[0]} lastTeam={data.semis[1]} />
                        <div>
                          <Box firstTeam={data.final[0]} lastTeam={data.final[1]} />
                        </div>
                        <Box firstTeam={data.semis[2]} lastTeam={data.semis[3]} />
                      </div>
                      <div className="semi-finals-right">
                        <Box firstTeam={data.quarters[4]} lastTeam={data.quarters[5]} />
                        <Box firstTeam={data.quarters[6]} lastTeam={data.quarters[7]} />
                      </div>
                    </div>
                    <div className="quarter-finals-right">
                      <Box firstTeam={data.octaves[8]} lastTeam={data.octaves[9]} />
                      <Box firstTeam={data.octaves[10]} lastTeam={data.octaves[11]} />
                      <Box firstTeam={data.octaves[12]} lastTeam={data.octaves[13]} />
                      <Box firstTeam={data.octaves[14]} lastTeam={data.octaves[15]} />
                    </div>
                  </div>
                  <div className="winner">
                    Vencedor: {data.winner[0]}
                  </div>
                </div>
              }
            </div>
          </div>
          : <Loader />
        }
      </div>
    )
  }
}