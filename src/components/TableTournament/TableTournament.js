import React from 'react';
import { Button, H3, Card, Elevation } from '@blueprintjs/core';
import { TEAMS } from '../utils';
import { CURRENT_ENDPOINT, HTTP_POST, HTTP_GET, HTTP_DELETE } from '../../endpoint';
import Loader from '../Loader/Loader';

export default class TableTournament extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      teams: [],
      loaded: false,
      doneTable: false
    }

    this.addTeams = this.addTeams.bind(this);
    this.showTeams = this.showTeams.bind(this);
    this.renderTeams = this.renderTeams.bind(this);
    this.deleteAllTeams = this.deleteAllTeams.bind(this);
    this.playGroup = this.playGroup.bind(this);
    this.deleteAllPlayoffs = this.deleteAllPlayoffs.bind(this);
    this.deleteBrackets = this.deleteBrackets.bind(this);
  }

  componentDidMount() {
    this.showTeams();
  }

  addTeams() {
    this.setState({
      loaded: false
    });

    let teams = 0;

    TEAMS.forEach(element => {
      fetch(CURRENT_ENDPOINT + '/add_team', {
        ...HTTP_POST,
        body: JSON.stringify(element)
      }).then(response => {
        return response.json();
      }).then(() => {
        teams = teams + 1;

        if (teams === 80) {
          this.showTeams();
        }
      })
    });
  }

  showTeams() {
    this.setState({
      loaded: false
    });

    fetch(CURRENT_ENDPOINT + '/teams', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then((data) => {

      const has = data.some((ele) => ele.victory);

      this.setState({
        teams: data,
        loaded: true,
        doneTable: has
      })

      this.props.done(data)
    })
  }

  deleteAllTeams() {
    this.setState({
      loaded: false
    });

    fetch(CURRENT_ENDPOINT + '/teams', {
      ...HTTP_DELETE
    }).then(response => {
      return response.json();
    }).then(() => {
      this.deleteAllPlayoffs();
    })
  }

  deleteAllPlayoffs() {
    fetch(CURRENT_ENDPOINT + '/playoffs', {
      ...HTTP_DELETE
    }).then(response => {
      return response.json();
    }).then(() => {
      this.deleteBrackets();
    });
  }

  deleteBrackets() {
    fetch(CURRENT_ENDPOINT + '/bracket', {
      ...HTTP_DELETE
    }).then(response => {
      return response.json();
    }).then(() => {
      this.setState({
        doneTable: false
      })

      this.props.done([])
      this.showTeams();
    });
  }

  playGroup() {
    this.setState({
      loaded: false
    });

    fetch(CURRENT_ENDPOINT + '/play_groups', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then(() => {
      this.setState({
        doneTable: true
      })

      this.showTeams();
      this.props.done(this.state.teams);
    })
  }

  renderTeams() {
    const { teams } = this.state;

    const groups = teams.reduce((acc, value) => {
      if (acc[value.group]) {
        acc[value.group].push(value);
      } else {
        acc[value.group] = [value];
      }

      return acc;
    }, {})

    return Object.entries(groups).map((data, index) => {
      const [key, dataGroup] = data;

      return (
        <Card elevation={Elevation.TWO} key={index} style={{
          margin: '10px', width: '400px',
          display: 'flex',
          justifyContent: 'center'
        }}>
          <div>
            <H3>{key}</H3>
            <table className="bp3-html-table .modifier">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Time</th>
                  <th>Grupo</th>
                  <th>Pontos</th>
                  <th>Vitórias</th>
                </tr>
              </thead>
              <tbody>
                {this.renderGroup(dataGroup)}
              </tbody>
            </table>
          </div>
        </Card>
      )
    })
  }

  renderGroup(dataGroup) {
    return dataGroup.map((value, index) => {
      const { id, team, group, points, victory } = value;

      return (
        <tr key={index}>
          <td>{id}</td>
          <td>{team}</td>
          <td>{group}</td>
          <td>{points}</td>
          <td>{victory}</td>
        </tr>
      )
    })
  }

  render() {
    const { loaded, teams, doneTable } = this.state;

    const hasTeams = teams.length === 0;

    return (
      <div>
        {loaded ?
          <div>
            <div style={{ margin: '10px' }}>
              <Button onClick={this.addTeams} disabled={!hasTeams}>TIMES RANDOM</Button>
              <Button onClick={this.playGroup} disabled={hasTeams || doneTable}>JOGAR FASES</Button>
              <Button onClick={this.deleteAllTeams} disabled={hasTeams}>LIMPAR TIMES</Button>
            </div>
            <div style={{
              display: 'flex',
              flexWrap: 'wrap'
            }}>
              {this.renderTeams()}
            </div>
          </div>
          : <Loader />
        }
      </div>
    )
  }
}