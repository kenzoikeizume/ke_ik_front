import React from 'react';
import { CURRENT_ENDPOINT, HTTP_GET, HTTP_POST } from '../../endpoint';
import { Button, Card, H3, Elevation } from '@blueprintjs/core';
import Loader from '../Loader/Loader';

export default class Playoffs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: true,
      teams: [],
      play: false,
      disablePlay: false
    }

    this.getTeams = this.getTeams.bind(this);
    this.renderPlayoffs = this.renderPlayoffs.bind(this);
    this.playPlayoffs = this.playPlayoffs.bind(this);
  }

  getTeams() {
    this.setState({
      loaded: false
    })

    fetch(CURRENT_ENDPOINT + '/playoffs', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then((data) => {
      const hasTeam = data.some((ele) => ele.id);
      const hasVictory = data.some((ele) => ele.victory);

      this.setState({
        teams: data,
        loaded: true,
        play: true,
        disablePlay: !hasTeam || hasVictory
      })
    })
  }

  renderPlayoffs() {
    const { groupTeams } = this.props;

    const groups = groupTeams.reduce((acc, value) => {
      if (acc[value.group]) {
        acc[value.group].push(value);
      } else {
        acc[value.group] = [value];
      }

      return acc;
    }, {})

    return Object.entries(groups).map((data, index) => {
      const [key, dataGroup] = data;

      return (
        <Card elevation={Elevation.TWO} key={index} style={{
          margin: '10px', width: '400px',
          display: 'flex',
          justifyContent: 'center'
        }}>
          <div>
            <H3>{key}</H3>
            <table className="bp3-html-table .modifier">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Time</th>
                  <th>Vitória</th>
                </tr>
              </thead>
              <tbody>
                {this.renderGroup(dataGroup)}
              </tbody>
            </table>
          </div>
        </Card>
      )
    })
  }

  renderGroup(dataGroup) {
    const { teams } = this.state;

    return dataGroup.map((value, index) => {
      const { team, id } = value;
      const findTeam = teams.find((ele) => ele.team_id == id);

      return (
        findTeam ?
          <tr key={index}>
            <td>{findTeam.team_id}</td>
            <td>{team}</td>
            <td>{findTeam.victory != null ? findTeam.victory ? 'ganhou' : 'perdeu' : '-'}</td>
          </tr> : null
      )
    })
  }

  playPlayoffs() {
    this.setState({
      loaded: false
    });

    const { teams } = this.state;
    const { groupTeams } = this.props;

    const groups = groupTeams.reduce((acc, value) => {
      const findTeam = teams.find((ele) => ele.team_id == value.id);

      if (findTeam) {
        if (acc[value.group]) {
          acc[value.group].push(findTeam);
        } else {
          acc[value.group] = [findTeam];
        }
      }


      return acc;
    }, {})

    let teamsCount = 0;

    Object.values(groups).forEach(element => {
      let payload = {
        fistTeam: element[0],
        lastTeam: element[1]
      }

      fetch(CURRENT_ENDPOINT + '/play_playoffs', {
        ...HTTP_POST,
        body: JSON.stringify(payload)
      }).then(response => {
        return response.json();
      }).then(() => {
        teamsCount = teamsCount + 1;
        
        if (teamsCount === 16) {
          this.addTeamsBracket();
        }
      })
    });
  }

  addTeamsBracket() {
    fetch(CURRENT_ENDPOINT + '/add_brackets', {
      ...HTTP_GET
    }).then(response => {
      return response.json();
    }).then(() => {
      this.getTeams();
    })
  }

  render() {
    const { loaded, play, disablePlay } = this.state;
    const { groupTeams } = this.props;

    return (
      <div>
        {loaded ?
          <div>
            <div style={{ margin: '10px' }}>
              <Button onClick={this.getTeams}>GET PLAYOFFS</Button>
              <Button onClick={this.playPlayoffs} disabled={!play || disablePlay || groupTeams.length === 0}>JOGAR PLAYOFFS</Button>
            </div>
            <div style={{
              display: 'flex',
              flexWrap: 'wrap'
            }}>
              {this.renderPlayoffs()}
            </div>
          </div>
          : <Loader />
        }
      </div>
    )
  }
}